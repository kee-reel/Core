#include "simplelinker.h"

#include "../../Interfaces/ipluginlinker.h"

#include <QApplication>
#include <QDesktopServices>

const QString PLUGIN_LINKER_INTERFACE_NAME = qobject_interface_iid<IPluginLinker*>();

SimpleLinker::SimpleLinker(QObject *parent, IReferenceDescriptorPtr coreDescr, QObject* appObject, QWeakPointer<IApplication> app) :
	QObject(parent),
	m_coreDescr(coreDescr),
	m_appObject(appObject),
	m_app(app),
	m_conflictAskId(0)
{
	connect(m_appObject, SIGNAL(onUserAnswered(quint32, quint16)), this, SLOT(onUserAnswered(quint32, quint16)));
}

SimpleLinker::~SimpleLinker()
{
}

ReferenceInstancePtr<IPlugin> SimpleLinker::addPlugin(IPluginHandlerPtr pluginHandler)
{
	ReferenceInstancePtr<IPlugin> instance;
	if(!pluginHandler.toStrongRef()->load())
		return instance;

	auto&& obj = pluginHandler.toStrongRef()->getInstance();
	auto plugin = qobject_cast<IPlugin*>(obj );
	if(!plugin)
	{
		pluginHandler.toStrongRef()->unload();
		return instance;
	}

	if(!plugin->pluginInit(pluginHandler.toStrongRef()->getUID(), pluginHandler.toStrongRef()->getMeta()))
	{
		return instance;
	}

	instance.reference()->setInstance(plugin->getDescriptor());
	return instance;
}

void SimpleLinker::onUserAnswered(quint32 askId, quint16 optionIndex)
{
	if(m_conflictAskId != askId)
		return;

	if(m_conflictedLinkerPlugins.empty())
	{
		if(!optionIndex)
		{
			QDesktopServices::openUrl(QUrl("https://gitlab.com/c4rb0n_un1t/MASS/wikis/home", QUrl::TolerantMode));
		}
		QApplication::exit();
	}
	else
	{
		Q_ASSERT(optionIndex < m_conflictedLinkerPlugins.size());
		auto plugin = m_conflictedLinkerPlugins.at(optionIndex);
		m_conflictedLinkerPlugins.clear();
	}
}

void SimpleLinker::init()
{
	Q_ASSERT(m_conflictedLinkerPlugins.empty());

	auto&& plugins = m_app.toStrongRef()->getPlugins();

	for (auto& plugin : plugins)
	{
		auto metaData = plugin.toStrongRef()->getMeta().toStrongRef()->value("MetaData");
		if(metaData.type() != QJsonValue::Object)
			continue;
		auto linkerMetaData = metaData.toObject().value(PLUGIN_LINKER_INTERFACE_NAME);
		if(linkerMetaData.type() != QJsonValue::Object)
			continue;
		auto linkerName = linkerMetaData.toObject().value("name");
		if(linkerName.type() != QJsonValue::String)
			continue;
		m_conflictedLinkerPlugins.append(QPair<QString, IPluginHandlerPtr>(linkerName.toString(), plugin));
	}

	if(m_conflictedLinkerPlugins.size() > 1)
	{
		QVariantList linkerNames;
		for(auto& linker : m_conflictedLinkerPlugins)
		{
			linkerNames.append(QString("%1 (%2)").arg(linker.first).arg(linker.second.toStrongRef()->getFileName()));
		}
		m_conflictAskId = m_app.toStrongRef()->askUser("Found multiple linker plugins, which one do you want to use?", linkerNames);
		return;
	}
	else if(m_conflictedLinkerPlugins.size() == 1)
	{
		linkPlugins(m_conflictedLinkerPlugins.first().second);
	}
	else
	{
		m_conflictAskId = m_app.toStrongRef()->askUser("Internal error: No linker plugin found", {"Open troubleshoot page and close app", "Close app"});
	}
}

void SimpleLinker::linkPlugins(IPluginHandlerPtr plugin)
{
	ReferenceInstancePtr<IPlugin> instance = addPlugin(plugin);
	Q_ASSERT(instance->isInited());
	Q_ASSERT(instance->getInstancesHandler().toStrongRef()->setReferences(INTERFACE(IApplication), {m_coreDescr}));
	Q_ASSERT(instance->getInstancesHandler().toStrongRef()->transitToReadyState());

	for(auto& pluginPair : m_plugins)
	{
		auto instancesHandler = pluginPair->second->getInstancesHandler();
		auto &&requiredReferences = instancesHandler.toStrongRef()->requiredReferences();
		//		qDebug() << "Referent" << pluginPair->second.toStrongRef()->descr().toStrongRef()->name() << "require" << requiredReferences;
		for (auto referenceIter = requiredReferences.begin(); referenceIter != requiredReferences.end(); ++referenceIter)
		{
			auto pluginsIter = m_pluginsInterfaces.find(referenceIter.key());
			if(pluginsIter != m_pluginsInterfaces.end())
			{
				//				qDebug() << "Reference" << pluginsIter.key().name();
				QList<IReferenceDescriptorPtr> list;
				auto referencesCount = referenceIter.value();
				auto &&pluginsList = pluginsIter.value();
				if(referencesCount == 0)
				{
					for(auto& plugin : pluginsList)
					{
						list.append(plugin.toStrongRef()->second.reference()->descr());
					}
				}
				else
				{
					for(auto i = 0; i < referencesCount; ++i)
					{
						list.append(pluginsList[i].toStrongRef()->second.reference()->descr());
					}
				}
				if(!instancesHandler.toStrongRef()->setReferences(referenceIter.key(), list))
				{
					qDebug() << "Can't set references for plugin";
				}
			}
		}
	}

	for(auto& pluginPair : m_plugins)
	{
		auto instancesHandler = pluginPair->second->getInstancesHandler();
		instancesHandler.toStrongRef()->transitToReadyState();
	}
}
